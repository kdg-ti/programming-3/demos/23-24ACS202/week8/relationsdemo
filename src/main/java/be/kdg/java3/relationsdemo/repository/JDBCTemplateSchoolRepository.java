package be.kdg.java3.relationsdemo.repository;

import be.kdg.java3.relationsdemo.domain.School;
import be.kdg.java3.relationsdemo.domain.Student;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("jdbctemplate")
public class JDBCTemplateSchoolRepository implements SchoolRepository{
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert schoolInserter;

    public JDBCTemplateSchoolRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.schoolInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("SCHOOLS")
                .usingGeneratedKeyColumns("ID");
    }

    public static School mapSchoolRow(ResultSet rs, int rowid) throws SQLException {
        return new School(rs.getInt("ID"),
                rs.getString("NAME"));
    }
    @Override
    public List<School> findAll() {
        List<School> schools = jdbcTemplate.query("SELECT * FROM SCHOOLS",
                JDBCTemplateSchoolRepository::mapSchoolRow);
        return schools;
    }

    @Override
    public School findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM SCHOOLS WHERE ID = ?",
                JDBCTemplateSchoolRepository::mapSchoolRow, id);
    }

    @Override
    public School createSchool(School school) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", school.getName());
        school.setId(schoolInserter.executeAndReturnKey(parameters).intValue());
        return school;
    }

    @Override
    public void updateSchool(School school) {
        jdbcTemplate.update("UPDATE SCHOOLS SET NAME=? WHERE ID=?",
                school.getName(), school.getId());
    }

    @Override
    @Transactional
    public void deleteSchool(int id) {
        jdbcTemplate.update("DELETE FROM ADDRESS WHERE STUDENT_ID " +
                "in (SELECT STUDENT_ID FROM STUDENTS WHERE SCHOOL_ID = ?)", id);
        jdbcTemplate.update("DELETE FROM STUDENTS WHERE SCHOOL_ID = ?", id);
        jdbcTemplate.update("DELETE FROM SCHOOLS WHERE ID=?", id);
    }
}
